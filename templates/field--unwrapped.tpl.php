<?php

/**
 * @file field--unwrapped.tpl.php
 * Template implementation to display the value of a field without additional wrapping.
 *
 * @see template_preprocess_field()
 * @see theme_field()
 *
 * @ingroup themeable
 */
?>
<?php foreach ($items as $delta => $item): ?>
  <?php print render($item); ?>
<?php endforeach; ?>
